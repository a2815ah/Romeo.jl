type Response
    data::String
    headers::Dict
 
    addContent::Function
    setHeader::Function
    getHeaders::Function
    getContents::Function
    getResponse::Function
 
    function Response()
        this = new ()
 
        this.data = ""
 
        this.headers = Dict{String, String}()
 
        this.addContent = function (append::String)
            this.data = this.data * append
        end
 
        this.setHeader = function (header::String, value::String)
            this.headers[header] = value
        end
 
        this.getHeaders = function ()
            headers = ""
 
            for (header, value) in this.headers
                headers = headers * header
 
                if length(value) > 0
                    headers = headers * ": " * value
                end
 
                headers = headers * "\n"
            end
 
            return headers
        end
 
        this.getContents = function ()
            return this.data
        end
 
        this.getResponse = function ()
            response = ""
 
            response = this.getHeaders() * "\n" * this.getContents()
 
            return response
        end
 
        return this
    end
end