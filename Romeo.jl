Pkg.add("UnicodePlots")
Pkg.add("RCall")
Pkg.add("PyCall")


using RCall

plot=R"plot"()
℘(x,y)=plot(x,y,ylab="")
∫=sum

℘(sin.(2π*x))

N=1000
dx=1./N
x=0:dx:1

∂(f)=begin n=length(f); (f[2:n]-f[1:(n-1)])./dx end

using PyCall
@pyimport os
@pyimport sys
@pyimport string as py_string
pyjoin=py_string.join

macro sh_str(x) 
	begin 
		R"system"(int=true,x) 
	end
end



@everywhere function whoami()
    println(myid(), gethostname())
end

for i in 1:2 remotecall_fetch(whoami, i) end
remotecall_fetch(whoami, 4)


Map(f)=x->map(f,x)
Spawn(f)=Map(y->@spawn(f(y)))

1:8 |> Spawn(x->gethostname()) |> Map(fetch)
xx= (1:8 |> Spawn(x->gethostname()))
xx |> Map(fetch)

nprocs()
nworkers()
gethostname()
myid()

#using ClusterManagers
#ClusterManagers.addprocs(SlurmManager(2),partition="mpp2_inter")

#addprocs(["lxlogin5.lrz.de"],exename="~/julia061/bin/julia",dir="~",tunnel=true)







